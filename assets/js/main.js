/*global $ */
/*eslint-env browser*/
/* Add active  class in home slider */
$(document).ready(function () {
    'use strict';
    

    $(".thirdForm").click(function () {
        $(this).toggleClass("active").siblings().removeClass("active");
    });
    $(".thirdForm .body ul li").click(function () {
        $(".thirdForm").removeClass("active");

    });
    $(window).click(function() {
        $(".thirdForm").removeClass("active");
    });
    $('.allpages .icons').click(function() {
        $(".allpages").toggleClass("active");
    });
    $('.thirdForm , .customSelect').click(function(event){
        event.stopPropagation();
    });

    $(".customSelect span").click(function (){
        
        var spanVal = $(this).html();
        $(this).closest(".thirdForm").find('p').html(spanVal);
        $(this).closest(".thirdForm").find('input').val(spanVal);
    });

});
/* Add active  class in asidenav  */
$(document).ready(function () {
    'use strict';
    

    $("header .navBtn").click(function () {
        $(".mobileNav").addClass("active");
    });
    $("aside.mobileNav .closeNav").click(function () {
        $(".mobileNav").removeClass("active");
    });
    
    $(window).click(function() {
        $(".mobileNav").removeClass("active");
    });
    
    $('.mobileNav,header .navBtn').click(function(event){
        event.stopPropagation();
    });
});
/* Add active  class active in userLogin  */
$(document).ready(function () {
    'use strict';
    

    $(".userLogin li").click(function () {
        $(this).addClass("active").siblings().removeClass("active");
    });
    
    $(window).click(function() {
        $(".userLogin li").removeClass("active");
    });
    
    $('.userLogin li,.listLogin').click(function(event){
        event.stopPropagation();
    });
});

//========================Sycn side =====================//
$(document).ready(function (){
    'use strict';
    $(".white-box li a").click(function (e){
        
         e.preventDefault();
          $("html, body").animate({
              scrollTop: $("#"+ $(this).data("scroll")).offset().top - 70
          }, 1000);
        //Add Class Active
        $(this).addClass("active").parent().siblings().find("a").removeClass("active");
     });
      // Sycn Navbar
      
      $(window).scroll(function (){
          $(".blogC_title h3 a").each(function (){
              if($(window).scrollTop() > ($(this).offset().top - 70)){
                  titleID=$(this).attr("id");
                  $(".white-box li ").removeClass("active");
                  $(".white-box li a[data-scroll='"+ titleID +"']").closest('li').addClass("active");
              }
          })
      })
  });
  //========================Sycn side =====================//
$(document).ready(function (){
    'use strict';
    $(".setting-box  li a").click(function (e){
        
         e.preventDefault();
         
          $("html, body").animate({
              scrollTop: $("#"+ $(this).data("scroll")).offset().top - 70
          }, 1000);
        //Add Class Active
        $(this).addClass("active").parent().siblings().find("a").removeClass("active");
     });
      // Sycn Navbar
      
      $(window).scroll(function (){
          $(".col-md-9 .setting-box").each(function (){
              if($(window).scrollTop() > ($(this).offset().top - 70)){
                  var titleID=$(this).attr("id");
                  $(".setting-box li a").removeClass("active");
                  $(".setting-box li a[data-scroll='"+ titleID +"']").addClass("active");
              }
          })
      })
  });

  /* affix */
  var target = $('.setting-box--nav')
    target.after('<div class="affix" id="affix"></div>')

    var affix = $('.affix')
    affix.append(target.clone(true))

    // Show affix on scroll.
    var element = document.getElementById('affix')
    if (element !== null) {
    var position = target.position()
    window.addEventListener('scroll', function () {
        var height = $(window).scrollTop()
        if (height > position.top) {
        target.css('visibility', 'hidden')
        affix.css('display', 'block')
        } else {
        affix.css('display', 'none')
        target.css('visibility', 'visible')
        }
    })
    }
  /* slider at mobile in home page */
$(document).ready(function (){
    'use strict';

    function reslider(){
        if ($('.container').width() <= 720 ){
            $('.control-slider').show();
            $(".steps .slider").slick({
                rtl: true,
                nextArrow:"",
                prevArrow:"",
                dots:true,
                infinite:false
            });
        }
    };
    reslider();
    $(window).resize(function(){     
        reslider();
    }); 
}); 
/* slider in cat bots */
$(document).ready(function (){
    'use strict';
    $(".slideBlog .slider").slick({
        rtl: true,
        nextArrow:".slideBlog .sliderControl .arrowNext",
        prevArrow:".slideBlog .sliderControl .arrowPrev",
        infinite:true
    });
    $(".partners-slider .slider").slick({
        rtl: true,
        nextArrow:".partners-slider .sliderControl .arrow.next",
        prevArrow:".partners-slider .sliderControl .arrow.prev",
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                
              }
            },
            {
              breakpoint: 700,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                
              }
            }
          ]
        
    });
    $(".slideImg .slider").slick({
        rtl: true,
        nextArrow:".slideImg .sliderControl .arrow.next",
        prevArrow:".slideImg .sliderControl .arrow.prev",
        infinite:true
    });
    $(".textCard .textSlider").slick({
        rtl: true,
        nextArrow:".textCard .sliderControl .arrowNext",
        prevArrow:".textCard .sliderControl .arrowPrev",
        infinite:true,
        slidesToShow:2,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 1199,
              settings: {
                slidesToShow: 1
              }
            },
             {
              breakpoint: 991,
              settings: {
                slidesToShow: 2
              }
            },
            {
             breakpoint: 630,
             settings: {
               slidesToShow: 1
             }
           }
            
          ]
    });
    $(".photCard .photoSlider").slick({
        rtl: true,
        nextArrow:".photCard .sliderControl .arrowNext",
        prevArrow:".photCard .sliderControl .arrowPrev",
        infinite:true,
        slidesToShow:2,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 1199,
              settings: {
                slidesToShow: 1
              }
            },
             {
              breakpoint: 991,
              settings: {
                slidesToShow: 2
              }
            },
            {
             breakpoint: 630,
             settings: {
               slidesToShow: 1
             }
           }
            
          ]
    });
    // $(".chat-history li.tags .slider").slick({
    //     rtl: true,
    //     nextArrow:"",
    //     prevArrow:"",
    //     slidesToShow: 3,
    //     slidesToScroll: 1,
        
    //     infinite:true
    // });
 
}); 
/* news-lg news-sm */
$(document).ready(function (){
    'use strict';
    $('.news-widget .foot-news a.changeGrid').click(function (e){
        e.preventDefault();
        $('.news-widget').addClass('news-sm').removeClass('news-lg');
        $(this).parent().parent().parent().addClass('news-lg').removeClass('news-sm');
        var numg = $(this).attr('data-num');
        console.log(numg);
        for(var i =0; i <= 5;i++){
          $('.gird-container').removeClass('style'+i);
          $('.grid'+i).addClass('animateGrid');
          $('.grid'+i).removeClass('animateGrid');
        }
        $('.gird-container').addClass('style'+numg);
        
        $('.grid'+numg).animate('transform', 'scale(0)');
    });
}); 
//========================Load=====================//
$(document).ready(function () {
    "use strict";
    $(window).on('load',function () {
        $('.allpages').removeClass('active');
        $(".load").fadeOut(100, function () {
            $("body").css("overflow", "auto");
            $(this).fadeOut(100, function () {
                $(this).remove();
            });
        });
    });

});
/* doctor search */
$(document).ready(function (){
    'use strict';
    $('.doctorOption .timestone ,.doctorOption .done').hide();
   $('.timestone .timed span').click(function (){
        $(this).closest('.timestone').find('span').removeClass('active');
        $(this).addClass('active');
        $(this).closest('.timestone').find('button').removeClass('active');
        $(this).closest('.time').find('button').addClass('active');
        var time = $(this).html();
        $('.timestone input').val(time);
   });
    
}); 
function book(){
    $('.needTo').hide();
    $('.doctorOption .book').hide();
    $('.doctorOption .timestone').show();
    $('.doctorOption .done').hide();
    $("html, body").animate({
        scrollTop: $('.timestone').offset().top - 60
    }, 500);
}
function consultation(){
    $('.needTo').hide();
    $('.doctorOption .consultation').hide();
    $('.doctorOption .done').show();
    $('.doctorOption .timestone').hide();
}
/* profile
======================== */
$(document).ready(function (){
    'use strict';
    
    $('.calender').datetimepicker({
        baseCls: "perfect-datetimepicker", 
        date: new Date(),
        viewMode: 'YMDHM',
        language: 'en', //I18N
        //date update event
        onDateChange: null,
        //clear button click event
        onClear: null,
        //ok button click event
        onOk: null,
        //close button click event
        onClose: null,
        //today button click event
        onToday: null
      });
      $('.calender2').datetimepicker({
        date: new Date(),
        viewMode: 'YMD',
        language: 'en', //I18N
        //date update event
        onDateChange: null,
        //clear button click event
        onClear: null,
        //ok button click event
        onOk: null,
        //close button click event
        onClose: null,
        //today button click event
        onToday: null
    });
    $('.Pconsultation').hide();
    $('.PbookOpp').hide()
    $('.bPconsultation').click( function (){
        $('.Pconsultation').slideDown();
        $('.PbookOpp').hide()
    });
    $('.bPbookOpp').click( function (){
        $('.Pconsultation').hide();
        $('.PbookOpp').slideDown()
    });
});
/* open megaMenu
==================== */
$(document).ready(function (){
    'use strict';
    $('.navLinks a:first-child').click(function (){
        $('.megaMenuLinks.first').toggleClass('active');
        $('.megaMenuLinks.second').removeClass('active');
    });
    $('.navLinks a:last-child').click(function (){
        $('.megaMenuLinks.second').toggleClass('active');
        $('.megaMenuLinks.first').removeClass('active');
    });
    $(window).click(function() {
        $(".megaMenuLinks").removeClass("active");
    });
    
    $('.navLinks,.megaMenuLinks').click(function(event){
        event.stopPropagation();
    });
});
/* cancelBooking
============================ */
$(document).ready(function (){
    'use strict';
    $('.cancelBooking').click(function (){
        $(this).closest('.cardContent').find('.confirmCancel').addClass('active');
        $(this).hide();
    });
    $('.confirmCancel .Cno').click(function (){
        $(this).closest('.confirmCancel').removeClass('active');
        $(this).closest('.cardContent').find('.cancelBooking').show();
    });
    // $('.cancelBooking').click(function (){
    //     swal({
    //         title: "ها انت متاكد  ؟?",
    //         text: "لو حذفت المعاد من المحتمل انك  لن تسطيع حجزه مره اخر",
    //         icon: "warning",
    //         buttons: true,
    //         dangerMode: true,
    //       })
    //       .then((willDelete) => {
    //         if (willDelete) {
    //           swal("تم الحذف بنجاح", {
    //             icon: "success",
    //           });
    //         } else {
    //           swal("مازال الحجز متاح");
    //         }
    //  });
    // });
});


/* quiz
======================== */
$(document).ready(function (){
    'use strict';
    $('.quizAnswers .answer').click(function (){
        $(this).addClass('active').siblings().removeClass('active');
        $("html, body").animate({
            scrollTop: $('.answerTorF').offset().top - 60
        }, 500);
    });
    
    // $('.quizControl button').click(function (e){
        
       
        
    // });
});
$(document).ready(function (){
    var allQuizs=0;
    $('.questions').each(function (){
        allQuizs+=1;
       
    })
    
    'use strict';
    $('.questions').hide();
    var quizData = $('.questions').attr('data-quiz'),
        numQuizData = parseInt(quizData);
    
    if(numQuizData == 1){
        $(".questions[data-quiz='1']").show();
    }
    
    $('.quizControl .next').click(function (){
       
        
        if(numQuizData == allQuizs){
           
            $(".questions[data-quiz='"+ numQuizData +"']").show();
            $("html, body").animate({
                scrollTop:  $(".questions[data-quiz='"+ numQuizData +"']").offset().top - 60
            }, 500);
        } else{
            numQuizData +=1;
            $('.questions').hide();
            $(".questions[data-quiz='"+ numQuizData +"']").show();
            $("html, body").animate({
                scrollTop:  $(".questions[data-quiz='"+ numQuizData +"']").offset().top - 60
            }, 500);
        }
    });
    $('.quizControl .prev').click(function (){
        
        
        if(numQuizData == 1){
           
            $(".questions[data-quiz='"+ numQuizData +"']").show();
            $("html, body").animate({
                scrollTop:  $(".questions[data-quiz='"+ numQuizData +"']").offset().top - 60
            }, 500);
        } else{
            numQuizData -=1;
            $('.questions').hide();
            $(".questions[data-quiz='"+ numQuizData +"']").show();
            $("html, body").animate({
                scrollTop:  $(".questions[data-quiz='"+ numQuizData +"']").offset().top - 60
            }, 500);
        }
    });
});
/* open chat on phone
***************************/ 
$(document).ready(function (){
    'use strict';
    // $('.openChatBot').click(function(){
    //     $('.fixedPhone').toggleClass('openBot');
    //     $(this).toggleClass('topFixed');
    // });

   
    var currentStep = $('.steps li.active a').html();
    $('.steps span').html(currentStep); 
    $('.steps li.active').prevAll().addClass('active');
   
}); 

/* draggable
========================== */
$(document).ready(function (){
    'use strict';
    $("#list1").dragsort({
        dragSelector: "div",
         dragBetween: true, 
         dragEnd: saveOrder,
          placeHolderTemplate: "<li class='placeHolder'><div></div></li>" 
        });

        function saveOrder() {
            var data = $("#list1 li").map(function() { return $(this).children().html(); }).get();
            $("input[name=list1SortOrder]").val(data.join("|"));
        };
});
//========================WOW=====================//
// Init WOW.js and get instance
var wow = new WOW();
wow.init();

$(function () {

    // Viewing Uploaded Picture On Setup Admin Profile
    function livePreviewPicture(picture)
    {
      if (picture.files && picture.files[0]) {
        var picture_reader = new FileReader();
        picture_reader.onload = function(event) {
          $('#uploaded2').attr('src', event.target.result);
        };
        picture_reader.readAsDataURL(picture.files[0]);
      }
    }
    
  
    $(' form .picture .input2').on('change', function () {
      $('#uploaded2').fadeIn();
      livePreviewPicture(this);
    });
  
});
$(function () {

    // Viewing Uploaded Picture On Setup Admin Profile
    function livePreviewPicture(picture)
    {
      if (picture.files && picture.files[0]) {
        var picture_reader = new FileReader();
        picture_reader.onload = function(event) {
          $('#uploaded').attr('src', event.target.result);
        };
        picture_reader.readAsDataURL(picture.files[0]);
      }
    }
    
  
    $(' form .picture .input1').on('change', function () {
      $('#uploaded').fadeIn();
      livePreviewPicture(this);
    });
  
});
$(function () {

    // Viewing Uploaded Picture On Setup Admin Profile
    function livePreviewPicture(picture)
    {
      if (picture.files && picture.files[0]) {
        var picture_reader = new FileReader();
        picture_reader.onload = function(event) {
          $('#uploaded').attr('src', event.target.result);
        };
        picture_reader.readAsDataURL(picture.files[0]);
      }
    }
    
  
    $(' form .picture .input').on('change', function () {
      $('#uploaded').fadeIn();
      livePreviewPicture(this);
    });
  
});
  
  
  const compose = (...fns) => fns.reduce((f, g) => (...args) => f(g(...args)))

  const validateFileSize = (params) => {
    // console.log((params.file.size/1024)/1024)
    if(!params.hasError && (params.file.size/1024)/1024 > 4) {
      return {
        ...params,
        hasError: true,
        errorMessage: "Maximum file size exceeded. Size limit is 4MB."
      }
    }
    return params;
  }
  
  const validateFileType = (params) => {
    let types = ["jpeg", "jpg", "png", "webp", "gif", "bmp"], acceptedTypes = new RegExp(types.join("|"))
    
    if(!params.hasError && !acceptedTypes.test(params.file.type)) {
      return {
        ...params,
        hasError: true,
        errorMessage: `Incorrect file type. Please try one of the following: ${types.join(',')}`
      }
    }
    return params;
  }
  
  const readFile = (params) => {
    if(!params.hasError)  {
      let reader = new FileReader();
  
      reader.onload = function(e) {
        var img = new Image();
        img.src = e.target.result;
        params.destination.appendChild(img);
      };
      reader.readAsDataURL(params.file);
    }
    return params;
  }
  
  const handleError = (params) => {
    if(params.hasError) {
      params.errorContainer.innerHTML = params.errorMessage;
    }
    return params;
  }
  
  function handleChange(e) {
    const errorContainer = document.querySelector('.error'), files = e.target.files, destination = document.querySelector('.results');
    errorContainer.innerHTML = '';
      destination.innerHTML = '';
    
      Object.keys(files).map(i => {
        // let file = files[i];
        let params = {
          file: files[i],
          hasError: false,
          errorMessage: '',
          destination,
          errorContainer
        }
        
        let validateFile = compose(
          readFile,
          handleError,
          validateFileType,
          validateFileSize
        )
        
        validateFile(params);
    })
  }
  
    


  $(function() {
    function init() {
          $('[data-behaviour="custom-upload-input"]').on('change', updateButton);
    };
    
    function updateButton(e) {
          var inputValue = $(e.currentTarget).val().split( '\\' ).pop()
          $('[data-element="custom-upload-button"]').text(inputValue)
          e.preventDefault;
        //   console.log(inputValue)
    };
    
    init()
  });