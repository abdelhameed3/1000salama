/* chat
==================== */
(function(){
  
    var chat = {
      messageToSend: '',
      messageResponses: [
        'اي كلام  هنختبر بيه الشات ملهوش اي لازمه احنا بنلعب ونجرب بس',
        'هناك حقيقة مثبتة منذ زمن طويل واله طويل طويل اوي كمان  مش عارف ازاي بس هنختبر الكلام  دا مش اكتر ولا اقل',
        'هاي ! كيف حالك مستعد تختبر الكلام دا معايا ولا هتنام',
        'ياعم شغال صدقني شغال سيبني في حالي بقي وروح   خلص الواجب ونام',
        'ههههههههههههه  ياسطا لا صدقني انت غلطان روح كمل  مذاكرة ونام',
        'تمام تمام تمام  ياعم اسكت  بقي كدا انت حلو  ولذيذ يعني '
      ],
      init: function() {
        this.cacheDOM();
        this.bindEvents();
        this.render();
      },
      cacheDOM: function() {
        this.$chatHistory = $('.chat-history');
        this.$button = $('button');
        this.$textarea = $('#message-to-send');
        this.$chatHistoryList =  this.$chatHistory.find('ul');
      },
      bindEvents: function() {
        this.$button.on('click', this.addMessage.bind(this));
        this.$textarea.on('keyup', this.addMessageEnter.bind(this));
      },
      render: function() {
        this.scrollToBottom();
        if (this.messageToSend.trim() !== '') {
          var template = Handlebars.compile( $("#message-template").html());
          var context = { 
            messageOutput: this.messageToSend,
            time: this.getCurrentTime()
          };
  
          this.$chatHistoryList.append(template(context));
          this.scrollToBottom();
          this.$textarea.val('');
          
          // responses
          var templateResponse = Handlebars.compile( $("#message-response-template").html());
          var contextResponse = { 
            response: this.getRandomItem(this.messageResponses),
            time: this.getCurrentTime()
          };
          
          setTimeout(function() {
            this.$chatHistoryList.append(templateResponse(contextResponse));
            this.scrollToBottom();
          }.bind(this), 500);
          
        }
        
      },
      
      addMessage: function() {
        this.messageToSend = this.$textarea.val()
        this.render();         
      },
      addMessageEnter: function(event) {
          // enter was pressed
          if (event.keyCode === 13) {
            this.addMessage();
          }
      },
      scrollToBottom: function() {
         this.$chatHistory.scrollTop(this.$chatHistory[0].scrollHeight);
      },
      getCurrentTime: function() {
        return new Date().toLocaleTimeString().
                replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");
      },
      getRandomItem: function(arr) {
        return arr[Math.floor(Math.random()*arr.length)];
      }
      
    };
    
    chat.init();
    
  
  })();
  
  
